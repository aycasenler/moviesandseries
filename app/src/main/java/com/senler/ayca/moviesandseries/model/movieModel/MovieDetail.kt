package com.senler.ayca.moviesandseries.model.movieModel

import com.senler.ayca.moviesandseries.model.ProductionCompanies
import com.senler.ayca.moviesandseries.model.ProductionCountries
import com.google.gson.annotations.SerializedName

data class MovieDetail(
    @SerializedName("adult")
    val adult : Boolean,
    @SerializedName("backdrop_path")
    val backdrop_path : String,
    @SerializedName("genres")
    val genres : List<GenresList>,
    @SerializedName("original_title")
    val original_title : String,
    @SerializedName("overview")
    val overview : String,
    @SerializedName("poster_path")
    val poster_path : String,
    @SerializedName("production_companies")
    val production_companies : List<ProductionCompanies>,
    @SerializedName("production_countries")
    val production_countries : List<ProductionCountries>,
    @SerializedName("release_date")
    val release_date : String,
    @SerializedName("title")
    val title : String,
    @SerializedName("status")
    val status : String,
    @SerializedName("vote_average")
    val vote_average : Double
)