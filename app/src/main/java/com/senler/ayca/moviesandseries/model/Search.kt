package com.senler.ayca.moviesandseries.model

import com.google.gson.annotations.SerializedName

data class Search(
    @SerializedName("results")
    val results : List<SearchResults>
)