package com.senler.ayca.moviesandseries.model.castModel

import com.google.gson.annotations.SerializedName

data class CastData(
 @SerializedName("cast")
 val cast : List<CastProperties>
)