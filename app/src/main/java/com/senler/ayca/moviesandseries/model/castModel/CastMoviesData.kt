package com.senler.ayca.moviesandseries.model.castModel

import com.google.gson.annotations.SerializedName

data class CastMoviesData(
    @SerializedName("cast")
    val cast : List<CastMoviesProperties>
)