package com.senler.ayca.moviesandseries.model.seriesModel

import com.google.gson.annotations.SerializedName

data class SeriesProperties(
    @SerializedName("name")
    val name: String,
    @SerializedName("poster_path")
    val poster_path: String,
    @SerializedName("backdrop_path")
    val backdrop_path: String,
    @SerializedName("id")
    val id : Int,
    @SerializedName("vote_average")
    val vote_average: Double
)