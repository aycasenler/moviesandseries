package com.senler.ayca.moviesandseries.model.movieModel

import com.google.gson.annotations.SerializedName

data class MoviesData(
    @SerializedName("results")
    val results : List<MoviesProperties>
)