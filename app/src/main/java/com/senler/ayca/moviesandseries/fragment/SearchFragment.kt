package com.senler.ayca.moviesandseries.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.adapter.SearchPersonAdapter
import com.senler.ayca.moviesandseries.adapter.SearchShowsAdapter
import com.senler.ayca.moviesandseries.model.Search
import com.senler.ayca.moviesandseries.model.SearchResults
import com.senler.ayca.moviesandseries.retrofit.ApiClient
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ChasingDots
import com.github.ybq.android.spinkit.style.CubeGrid
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchFragment : Fragment(), TextWatcher, View.OnClickListener {
    lateinit var searchEt: EditText
    lateinit var searchShowsRecyclerView: RecyclerView
    lateinit var searchPersonRecyclerView: RecyclerView
    lateinit var searchPersonAdapter: SearchPersonAdapter
    lateinit var personList: MutableList<SearchResults>
    lateinit var showsList: MutableList<SearchResults>
    lateinit var searchShowsAdapter: SearchShowsAdapter
    private var isMovie: Boolean = true
    lateinit var progressBar: ProgressBar
    lateinit var blurLayout: LinearLayout
    lateinit var backBtn: ImageButton
    lateinit var adView: AdView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.search_fragment, container, false)
        initView(view)
        initAd()
        backPressHandler()
        return view
    }

    private fun initView(view: View) {
        adView = view.findViewById(R.id.ad_banner_search_fragment)
        searchEt = view.findViewById(R.id.search_et)

        searchShowsRecyclerView = view.findViewById(R.id.search_shows_recycler_view)
        searchShowsRecyclerView.layoutManager =
            GridLayoutManager(context, 3)

        searchPersonRecyclerView = view.findViewById(R.id.search_person_recycler_view)
        searchPersonRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        personList = mutableListOf()

        showsList = mutableListOf()

        progressBar = view.findViewById(R.id.spin_kit)
        blurLayout = view.findViewById(R.id.blur_layout)
        val cubeGrid: Sprite = CubeGrid()
        progressBar.setIndeterminateDrawableTiled(cubeGrid)

        backBtn = view.findViewById(R.id.back_btn)

        searchEt.addTextChangedListener(this)
        backBtn.setOnClickListener(this)

    }

    private fun search() {
        personList.clear()
        showsList.clear()
        val query = searchEt.text.toString().trim()
        val call: Call<Search> = ApiClient.getClient.getSearch(query)
        call.enqueue(object : Callback<Search> {
            override fun onFailure(call: Call<Search>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<Search>?, response: Response<Search>?) =
                if (response!!.isSuccessful) {
                    for (i in response.body().results) {
                        when (i.media_type) {
                            "person" -> {
                                if (!i.profile_path.isNullOrEmpty()) {
                                    personList.add(i)
                                }
                            }
                            "movie" -> {
                                if (!i.poster_path.isNullOrEmpty()) {
                                    showsList.add(i)
                                    isMovie = true
                                }
                            }
                            "tv" -> {
                                if (!i.poster_path.isNullOrEmpty()) {
                                    showsList.add(i)
                                    isMovie = false
                                }
                            }
                        }
                    }
                    searchPersonAdapter =
                        SearchPersonAdapter(personList, context!!)
                    searchPersonRecyclerView.adapter = searchPersonAdapter
                    searchShowsAdapter =
                        SearchShowsAdapter(showsList, context!!, isMovie)
                    searchShowsRecyclerView.adapter = searchShowsAdapter
                    blurLayout.visibility = View.GONE
                } else {
                }
        }
        )
    }

    override fun afterTextChanged(s: Editable?) {
        blurLayout.visibility = View.VISIBLE
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        search()
    }

    override fun onClick(v: View?) {
        Navigation.findNavController(v!!).navigate(R.id.homeScreenFragment)
    }

    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view!!).popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    private fun initAd() {
        MobileAds.initialize(context, "ca-app-pub-1898235865477290~9103030179")
        val adRequest = AdRequest.Builder().build()
        adView?.loadAd(adRequest)
    }
}