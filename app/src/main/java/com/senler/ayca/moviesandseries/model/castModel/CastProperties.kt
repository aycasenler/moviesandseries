package com.senler.ayca.moviesandseries.model.castModel

import com.google.gson.annotations.SerializedName

data class CastProperties(
    @SerializedName("name")
    val name: String,
    @SerializedName("character")
    val character: String,
    @SerializedName("profile_path")
    val profile_path: String,
    @SerializedName("id")
    val id: Int
)