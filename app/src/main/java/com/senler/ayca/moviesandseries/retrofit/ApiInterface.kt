package com.senler.ayca.moviesandseries.retrofit

import com.senler.ayca.moviesandseries.model.*
import com.senler.ayca.moviesandseries.model.castModel.CastData
import com.senler.ayca.moviesandseries.model.castModel.CastMoviesData
import com.senler.ayca.moviesandseries.model.castModel.CastSeriesData
import com.senler.ayca.moviesandseries.model.castModel.PersonalInformations
import com.senler.ayca.moviesandseries.model.movieModel.MovieDetail
import com.senler.ayca.moviesandseries.model.movieModel.MoviesData
import com.senler.ayca.moviesandseries.model.review.Review
import com.senler.ayca.moviesandseries.model.seriesModel.SeasonsDetail
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesData
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesDetail
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @GET("movie/popular?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getPopularMovies(): Call<MoviesData>

    @GET("movie/top_rated?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getTopRatedMovies(): Call<MoviesData>

    @GET("movie/upcoming?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getUpComingMovies(): Call<MoviesData>

    @GET("movie/{movie_id}?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getMovieDetail(@Path("movie_id") movieId: Int): Call<MovieDetail>

    @GET("movie/{movie_id}/similar?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getSimilarMovies(@Path("movie_id") movieId: Int): Call<MoviesData>


    @GET("movie/{movie_id}/videos?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getVideo(@Path("movie_id") movieId: Int): Call<VideoData>

    @GET("movie/{movie_id}/credits?api_key=36ddc17ac8efd52e16e783c51ef16255")
    fun getMovieCast(@Path("movie_id") movieId: Int): Call<CastData>

    @GET("movie/{movie_id}/reviews?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getMovieReviews(@Path("movie_id") movieId: Int): Call<Review>


    @GET("tv/popular?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getPopularSeries(): Call<SeriesData>

    @GET("tv/top_rated?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getTopRatedSeries(): Call<SeriesData>

    @GET("tv/on_the_air?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getOnTheAirSeries(): Call<SeriesData>

    @GET("tv/airing_today?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getOnAiringTodaySeries(): Call<SeriesData>

    @GET("tv/{tv_id}?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getSeriesDetail(@Path("tv_id") seriesId: Int): Call<SeriesDetail>

    @GET("tv/{tv_id}/credits?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getSeriesCast(@Path("tv_id") seriesId: Int): Call<CastData>

    @GET("tv/{tv_id}/reviews?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getSeriesReviews(@Path("tv_id") seriesId: Int): Call<Review>

    @GET("tv/{tv_id}/season/{season_number}/episode/{episode_number}/videos?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getEpisodeVideo(
        @Path("tv_id") seriesId: Int,
        @Path("season_number") seasonNumber: Int,
        @Path("episode_number") episodeNumber: Int
    ): Call<VideoData>

    @GET("tv/{tv_id}/season/{season_number}?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getEpisodeDetail(
        @Path("tv_id") seriesId: Int,
        @Path("season_number") seasonsNumber: Int
    ): Call<SeasonsDetail>

    @GET("person/{person_id}?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getPersonalInformations(@Path("person_id") personId: Int): Call<PersonalInformations>

    @GET("person/{person_id}/movie_credits?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getCastMovies(@Path("person_id") personId: Int): Call<CastMoviesData>

    @GET("person/{person_id}/tv_credits?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getCastSeries(@Path("person_id") personId: Int): Call<CastSeriesData>

    @GET("search/multi?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&query=brad&page=1&include_adult=false")
    fun getSearch(@Query("query") query: String): Call<Search>

    @GET("authentication/token/new?api_key=36ddc17ac8efd52e16e783c51ef16255")
    fun getToken(): Call<Token>

    @GET("authentication/session/new?api_key=36ddc17ac8efd52e16e783c51ef16255")
    fun getSession(@Query("request_token") request_token: String): Call<Session>

    @GET("authentication/token/validate_with_login?api_key=36ddc17ac8efd52e16e783c51ef16255")
    fun createSessionWithLogin(
        @Query("username") username: String,
        @Query("password") password: String,
        @Query("request_token") request_token: String
    ): Call<Token>

    @GET("account?api_key=36ddc17ac8efd52e16e783c51ef16255")
    fun getAccountDetail(@Query("session_id") session_id: String): Call<Account>

    @GET("account/{account_id}/favorite/movies?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&sort_by=created_at.asc&page=1")
    fun getFavoriteMovies(
        @Path("account_id") account_id: Int,
        @Query("session_id") session_id: String
    ): Call<MoviesData>

    @GET("account/{account_id}/favorite/tv?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&sort_by=created_at.asc&page=1")
    fun getFavoriteSeries(
        @Path("account_id") account_id: Int,
        @Query("session_id") session_id: String
    ): Call<SeriesData>

    @GET("account/{account_id}/watchlist/movies?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&sort_by=created_at.asc&page=1")
    fun getMovieWatchlist(
        @Path("account_id") account_id: Int,
        @Query("session_id") session_id: String
    ): Call<MoviesData>

    @GET("account/{account_id}/watchlist/tv?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&sort_by=created_at.asc&page=1")
    fun getSeriesWatchlist(
        @Path("account_id") account_id: Int,
        @Query("session_id") session_id: String
    ): Call<SeriesData>

    @POST("account/{account_id}/favorite?api_key=36ddc17ac8efd52e16e783c51ef16255")
    fun markAsFavorite(
        @Path("account_id") account_id: Int,
        @Query("session_id") session_id: String,
        @Body markFavoriteShows: MarkFavoriteShows
    ): Call<MarkAsFavorite>

    @POST("account/{account_id}/watchlist?api_key=36ddc17ac8efd52e16e783c51ef16255")
    fun addToWatchlist(
        @Path("account_id") account_id: Int,
        @Query("session_id") session_id: String,
        @Body addShowsToWatchlist: AddShowsToWatchlist
    ): Call<AddtoWatchlist>

    @GET("genre/movie/list?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getMovieGenreList(): Call<Genres>

    @GET("genre/tv/list?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getSeriesGenreList(): Call<Genres>

    @GET("discover/movie?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&sort_by=popularity.desc")
    fun getMovieListByGenre(
        @Query("with_genres") with_genres: String,
        @Query("page") page: Int
    ): Call<MoviesData>

    @GET("discover/tv?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&sort_by=popularity.desc&page=1&")
    fun getSeriesListByGenre(
        @Query("with_genres") with_genres: String,
        @Query("page") page: Int
    ): Call<SeriesData>
}