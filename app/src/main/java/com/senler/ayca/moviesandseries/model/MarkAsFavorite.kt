package com.senler.ayca.moviesandseries.model

import com.google.gson.annotations.SerializedName

data class MarkAsFavorite(
    @SerializedName("status_message")
    val status_message : String,
    @SerializedName("status_code")
    val status_code : Int
)