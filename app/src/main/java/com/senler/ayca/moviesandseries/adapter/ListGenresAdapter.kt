package com.senler.ayca.moviesandseries.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.GenreProperties

class ListGenresAdapter(
    private var genresList: List<GenreProperties>,
    private var context: Context,
    private var genreIdList: ArrayList<Int>,
    private var selectedPositionList: ArrayList<Int>,
    private var isMovie: Boolean


) :
    RecyclerView.Adapter<ListGenresAdapter.ViewHolder>() {
    private var selectedItem = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.genres_list_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return genresList.size
    }

    override fun onBindViewHolder(holder: ListGenresAdapter.ViewHolder, position: Int) {

        val genres = genresList[position]
        holder.genresBtn.text = genres.name
        val bundle = Bundle()

        val backgroundStrokeColor = holder.genresBtn.background as GradientDrawable
        holder.genresBtn.setOnClickListener {
            selectedItem = genres.id

            notifyDataSetChanged()

            if (!genreIdList.removeIf { genreIdList.contains(genres.id) }) {
                genreIdList.add(genres.id)

            }
            if (selectedPositionList.contains(position))
                selectedPositionList.remove(position)
            else
                selectedPositionList.add(position)
            bundle.putIntegerArrayList("genreIdList", genreIdList)
            bundle.putIntegerArrayList("selectedPositionList", selectedPositionList)
            if (isMovie)
                Navigation.findNavController(it).navigate(R.id.listMoviesByGenreFragment, bundle)
            else
                Navigation.findNavController(it).navigate(R.id.listSeriesByGenreFragment, bundle)

        }

        if (selectedPositionList.contains(position))
            backgroundStrokeColor.setStroke(5, Color.parseColor("#D81B60"))
        else
            backgroundStrokeColor.setStroke(5, Color.parseColor("#FFFFFFFF"))


    }


    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var genresBtn: Button = itemLayoutView.findViewById(R.id.genres_btn)

    }

}


