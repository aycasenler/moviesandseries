package com.senler.ayca.moviesandseries.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.SearchResults
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class SearchPersonAdapter (
    private var searchList: List<SearchResults>,
    private var context: Context
) :
    RecyclerView.Adapter<SearchPersonAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.cast_list_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return searchList.size
    }

    override fun onBindViewHolder(holder: SearchPersonAdapter.ViewHolder, position: Int) {

        val search = searchList[position]

        if (search.profile_path.isNullOrEmpty()) {

            holder.personNameTv.width = 0
            holder.personLL.layoutParams.width = 0
            holder.personLL.layoutParams.height = 0

        } else {
            val url =
                "https://image.tmdb.org/t/p/w500" + search.profile_path
            picassoImage(holder.personImg, url)
            holder.personNameTv.text = search.name
            holder.personNameTv.width = 350
            holder.personLL.layoutParams.width = 350
            holder.personLL.layoutParams.height = 450
        }


        holder.itemView.setOnClickListener {
            clickCast(position, it)
        }

    }

    private fun clickCast(position: Int, view: View) {
        val bundle = Bundle()
        bundle.putInt("personId", searchList[position].id)
        Navigation.findNavController(view).navigate(R.id.personDetailFragment, bundle)
    }

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var personImg: ImageView = itemLayoutView.findViewById(R.id.cast_img)
        var personNameTv: TextView = itemLayoutView.findViewById(R.id.cast_name_tv)
        var personLL: LinearLayout = itemLayoutView.findViewById(R.id.cast_ll)

    }

    fun picassoImage(imageView: ImageView, url: String) {
        Picasso.get().load(url).transform(CropCircleTransformation()).into(imageView)
    }
}