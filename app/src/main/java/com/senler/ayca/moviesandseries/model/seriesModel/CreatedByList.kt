package com.senler.ayca.moviesandseries.model.seriesModel

import com.google.gson.annotations.SerializedName

data class CreatedByList(
    @SerializedName("id")
    val id : Int,
    @SerializedName("name")
    val name :String,
    @SerializedName("profile_path")
    val profile_path : String
)