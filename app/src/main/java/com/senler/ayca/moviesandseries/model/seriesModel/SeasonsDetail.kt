package com.senler.ayca.moviesandseries.model.seriesModel

import com.google.gson.annotations.SerializedName

data class SeasonsDetail(
    @SerializedName("episodes")
    val episodes : List<Episodes>
)