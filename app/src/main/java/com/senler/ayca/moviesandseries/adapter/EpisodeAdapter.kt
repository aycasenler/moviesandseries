package com.senler.ayca.moviesandseries.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.VideoData
import com.senler.ayca.moviesandseries.model.seriesModel.Episodes
import com.senler.ayca.moviesandseries.model.seriesModel.Seasons
import com.senler.ayca.moviesandseries.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EpisodeAdapter (private var episodeList: List<Episodes>, private var context: Context, private var seriesId: Int) :
    RecyclerView.Adapter<EpisodeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.episode_list_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return episodeList.size
    }

    override fun onBindViewHolder(holder: EpisodeAdapter.ViewHolder, position: Int) {

        val episode = episodeList[position]

        holder.episodeNumberTv.text = episode.episode_number.toString()
        holder.episodeNameTv.text = episode.name

        holder.trailerTv.setOnClickListener {
            getEpisodeVideo(seriesId, episode.season_number, episode.episode_number)
        }
    }

    private fun getEpisodeVideo(seriesId: Int, seasonNumber: Int, episodeNumber: Int) {
        val call: Call<VideoData> = ApiClient.getClient.getEpisodeVideo(seriesId,seasonNumber,episodeNumber)
        call.enqueue(object : Callback<VideoData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<VideoData>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT).show()

            }

            override fun onResponse(call: Call<VideoData>?, response: Response<VideoData>?) =
                if (response!!.isSuccessful) {

                        if(response.body().results.isNullOrEmpty()){
                            Toast.makeText(context,R.string.video_not_found,Toast.LENGTH_SHORT).show()
                        }else{
                            val videoKey = response.body().results[0].key
                            val uri = Uri.parse("https://www.youtube.com/watch?v=$videoKey")
                            val intent = Intent(Intent.ACTION_VIEW, uri)
                            context.startActivity(intent)
                        }


                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }


    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var episodeNumberTv : TextView = itemLayoutView.findViewById(R.id.episode_number_tv)
        var episodeNameTv : TextView = itemLayoutView.findViewById(R.id.episode_name_tv)
        var trailerTv : TextView = itemLayoutView.findViewById(R.id.trailer_tv)

    }


}