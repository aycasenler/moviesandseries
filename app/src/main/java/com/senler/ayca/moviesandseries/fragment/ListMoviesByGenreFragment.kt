package com.senler.ayca.moviesandseries.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.adapter.ListGenresAdapter
import com.senler.ayca.moviesandseries.adapter.MoviesAdapter
import com.senler.ayca.moviesandseries.model.Genres
import com.senler.ayca.moviesandseries.model.GenreProperties
import com.senler.ayca.moviesandseries.model.movieModel.MoviesData
import com.senler.ayca.moviesandseries.model.movieModel.MoviesProperties
import com.senler.ayca.moviesandseries.retrofit.ApiClient
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ChasingDots
import com.github.ybq.android.spinkit.style.Circle
import com.github.ybq.android.spinkit.style.CubeGrid
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListMoviesByGenreFragment : Fragment(), View.OnClickListener {
    lateinit var genresListRecyclerView: RecyclerView
    lateinit var listShowsRecyclerView: RecyclerView
    lateinit var listGenresAdapter: ListGenresAdapter
    lateinit var genresList: List<GenreProperties>
    lateinit var movieAdapter: MoviesAdapter
    lateinit var movieList: MutableList<MoviesProperties>
    lateinit var showTxt: TextView
    lateinit var backBtn: ImageButton
    lateinit var progressBar : ProgressBar
    lateinit var blurLayout : LinearLayout
    lateinit var adView: AdView
    var genresIdString = ""
    var visibleItemCount: Int = 0
    var totalItemcount: Int = 0
    var pastVisibleItemCount: Int = 0
    var loading: Boolean = false
    var page: Int = 1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.list_by_genre_fragment, container, false)
        initView(view)
        initAd()
        backPressHandler()
        return view
    }
    private fun initAd() {
        MobileAds.initialize(context, "ca-app-pub-1898235865477290~9103030179")
        val adRequest = AdRequest.Builder().build()
        adView?.loadAd(adRequest)
    }
    fun initView(view: View) {
        adView = view.findViewById(R.id.ad_banner_list_by_genre_fragment)
        genresListRecyclerView = view.findViewById(R.id.genres_list_recycler_view)
        genresListRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        listShowsRecyclerView = view.findViewById(R.id.list_shows_recycler_view)
        listShowsRecyclerView.layoutManager =
            GridLayoutManager(context, 3)
        showTxt = view.findViewById(R.id.show_txt)
        showTxt.text = "Movies"
        backBtn = view.findViewById(R.id.back_btn)
        genresList = mutableListOf()
        movieList = mutableListOf()

        progressBar = view.findViewById(R.id.spin_kit)
        blurLayout = view.findViewById(R.id.blur_layout)
        val cubeGrid: Sprite = CubeGrid()
        progressBar.setIndeterminateDrawableTiled(cubeGrid)

        listGenres()

        backBtn.setOnClickListener(this)
    }

    private fun listGenres() {
        val call: Call<Genres> = ApiClient.getClient.getMovieGenreList()
        call.enqueue(object : Callback<Genres> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<Genres>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<Genres>?, response: Response<Genres>?) =
                if (response!!.isSuccessful) {
                    genresList = response.body().genres
                    var selectedPositionList =
                        this@ListMoviesByGenreFragment.arguments?.getIntegerArrayList("selectedPositionList")

                    var genreIdList =
                        this@ListMoviesByGenreFragment.arguments?.getIntegerArrayList("genreIdList")
                    if (genreIdList == null) {
                        genreIdList = ArrayList<Int>()

                    }
                    if (selectedPositionList == null) {
                        selectedPositionList = ArrayList<Int>()

                    }

                    listGenresAdapter =
                        ListGenresAdapter(
                            genresList,
                            context!!,
                            genreIdList,
                            selectedPositionList,
                            true
                        )

                    genresListRecyclerView.adapter = listGenresAdapter


                    for (i in genreIdList) {
                        if (genreIdList.isNotEmpty())
                            genresIdString += "$i,"
                        else
                            genresIdString += i
                    }

                    getMovieListByGenre(genresIdString, page)

                } else {
                }
        }
        )
    }

    fun getMovieListByGenre(genreId: String, page: Int) {
        val call: Call<MoviesData> = ApiClient.getClient.getMovieListByGenre(genreId, page)
        call.enqueue(object : Callback<MoviesData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<MoviesData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<MoviesData>?, response: Response<MoviesData>?) =
                if (response!!.isSuccessful) {
                    progressBar.visibility = View.GONE
                    loading = true
                    setUpAdapter(response.body())
                    blurLayout.visibility = View.GONE
                } else {
                }
        }
        )
    }

    private fun setUpAdapter(body: MoviesData?) {
        if (movieList.size == 0) {
            movieList = body!!.results as MutableList<MoviesProperties>
            movieAdapter =
                MoviesAdapter(movieList, context!!)

            listShowsRecyclerView.adapter = movieAdapter
        } else {
            var currentPosition =
                (listShowsRecyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
            movieList.addAll(body!!.results)
            movieAdapter.notifyDataSetChanged()
            listShowsRecyclerView.scrollToPosition(currentPosition)
        }


        listShowsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    visibleItemCount = listShowsRecyclerView.layoutManager!!.childCount
                    totalItemcount = listShowsRecyclerView.layoutManager!!.itemCount
                    pastVisibleItemCount =
                        (listShowsRecyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                    if (loading) {
                        if ((visibleItemCount + pastVisibleItemCount) >= totalItemcount) {
                            loading = false
                            progressBar.visibility = View.VISIBLE
                            page++
                            getMovieListByGenre(genresIdString, page)
                        }
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })

    }

    override fun onClick(v: View?) {
        Navigation.findNavController(v!!).navigate(R.id.homeScreenFragment)
    }
    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view!!).popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}