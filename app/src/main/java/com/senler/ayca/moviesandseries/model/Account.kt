package com.senler.ayca.moviesandseries.model

import com.google.gson.annotations.SerializedName

data class Account(
    @SerializedName("username")
    val username: String,
    @SerializedName("id")
    val id: Int
)