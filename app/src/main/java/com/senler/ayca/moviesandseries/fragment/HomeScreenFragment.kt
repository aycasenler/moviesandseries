package com.senler.ayca.moviesandseries.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Vibrator
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.interfaces.ItemClickListener
import com.denzcoskun.imageslider.models.SlideModel
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.adapter.MoviesAdapter
import com.senler.ayca.moviesandseries.adapter.SeriesAdapter
import com.senler.ayca.moviesandseries.model.movieModel.MoviesData
import com.senler.ayca.moviesandseries.model.movieModel.MoviesProperties
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesData
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesProperties
import com.senler.ayca.moviesandseries.retrofit.ApiClient
import com.github.florent37.viewtooltip.ViewTooltip
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ChasingDots
import com.github.ybq.android.spinkit.style.CubeGrid
import com.github.ybq.android.spinkit.style.Pulse
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import net.cachapa.expandablelayout.ExpandableLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlinx.android.synthetic.main.custom_dialog_design.view.*

class HomeScreenFragment : Fragment(), View.OnClickListener {
    lateinit var popularMoviesRecyclerView: RecyclerView
    lateinit var topRatedMoviesRecyclerView: RecyclerView
    lateinit var upComingMoviesRecyclerView: RecyclerView
    lateinit var popularSeriesRecyclerView: RecyclerView
    lateinit var topRatedSeriesRecyclerView: RecyclerView
    lateinit var onTheAirSeriesRecyclerView: RecyclerView
    lateinit var onAiringTodaySeriesRecyclerView: RecyclerView
    lateinit var moviesAdapter: MoviesAdapter
    lateinit var seriesAdapter: SeriesAdapter
    lateinit var moviesList: List<MoviesProperties>
    lateinit var seriesList: List<SeriesProperties>
    lateinit var searchBtn: ImageButton
    lateinit var loginBtn: ImageButton
    lateinit var favoriteListBtn: Button
    lateinit var watchlistBtn: Button
    lateinit var userPropertiesPreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var listMoviesBtn: Button
    lateinit var listSeriesBtn: Button
    lateinit var list: MutableList<String>
    lateinit var progressBar: ProgressBar
    lateinit var blurLayout: LinearLayout
    lateinit var accountDetailExpandableLayout: ExpandableLayout
    lateinit var usernameTxt: TextView
    lateinit var logoutBtn: Button
    lateinit var imageSlider: ImageSlider
    private var imageList = ArrayList<SlideModel>()
    private var bodyMoviesData: MoviesData? = null
    private var bodySeriesData: SeriesData? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.home_screen_fragment, container, false)
        initview(view)
        backPressHandler()
        return view
    }

    private fun initview(view: View) {
        popularMoviesRecyclerView = view.findViewById(R.id.popular_movies_recycler_view)
        popularMoviesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        topRatedMoviesRecyclerView = view.findViewById(R.id.top_rated_movies_recycler_view)
        topRatedMoviesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        upComingMoviesRecyclerView = view.findViewById(R.id.up_coming_movies_recycler_view)
        upComingMoviesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        popularSeriesRecyclerView = view.findViewById(R.id.popular_series_recycler_view)
        popularSeriesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        topRatedSeriesRecyclerView = view.findViewById(R.id.top_rated_series_recycler_view)
        topRatedSeriesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        onTheAirSeriesRecyclerView = view.findViewById(R.id.on_the_air_series_recycler_view)
        onTheAirSeriesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        onAiringTodaySeriesRecyclerView =
            view.findViewById(R.id.on_airing_today_series_recycler_view)
        onAiringTodaySeriesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        searchBtn = view.findViewById(R.id.search_btn)
        loginBtn = view.findViewById(R.id.login_btn)
        moviesList = mutableListOf()
        favoriteListBtn = view.findViewById(R.id.favorite_list_btn)
        watchlistBtn = view.findViewById(R.id.watchlist_btn)
        accountDetailExpandableLayout = view.findViewById(R.id.account_detail_expandable_view)
        accountDetailExpandableLayout.expand()
        accountDetailExpandableLayout.collapse()
        usernameTxt = view.findViewById(R.id.username_txt)
        logoutBtn = view.findViewById(R.id.logout_btn)
        userPropertiesPreferences =
            context!!.getSharedPreferences("userProperties", Context.MODE_PRIVATE)
        editor = userPropertiesPreferences.edit()

        listMoviesBtn = view.findViewById(R.id.list_movies_btn)
        listSeriesBtn = view.findViewById(R.id.list_series_btn)

        imageSlider = view.findViewById(R.id.image_slider)

        progressBar = view.findViewById(R.id.spin_kit)
        blurLayout = view.findViewById(R.id.blur_layout)

        val session = userPropertiesPreferences.getString("sessionId", "")

        if (!session.isNullOrEmpty())
            loginBtn.setImageResource(R.drawable.user_login)


        val cubeGrid: Sprite = CubeGrid()
        progressBar.setIndeterminateDrawableTiled(cubeGrid)

        getPopularMovies(view)
        getTopRatedMovies()
        getUpComingMovies()

        getPopularSeries(view)
        getTopRatedSeries()
        getOnTheAirSeries()
        getOnAiringTodaySeries()

        searchBtn.setOnClickListener(this)
        loginBtn.setOnClickListener(this)
        favoriteListBtn.setOnClickListener(this)
        watchlistBtn.setOnClickListener(this)
        listSeriesBtn.setOnClickListener(this)
        listMoviesBtn.setOnClickListener(this)
        logoutBtn.setOnClickListener(this)

    }

    private fun getOnAiringTodaySeries() {
        val call: Call<SeriesData> = ApiClient.getClient.getOnAiringTodaySeries()
        call.enqueue(object : Callback<SeriesData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<SeriesData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<SeriesData>?, response: Response<SeriesData>?) =
                if (response!!.isSuccessful) {

                    seriesList = response.body().results
                    seriesAdapter =
                        SeriesAdapter(seriesList, context!!)

                    onAiringTodaySeriesRecyclerView.adapter = seriesAdapter
                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getOnTheAirSeries() {
        val call: Call<SeriesData> = ApiClient.getClient.getOnTheAirSeries()
        call.enqueue(object : Callback<SeriesData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<SeriesData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<SeriesData>?, response: Response<SeriesData>?) =
                if (response!!.isSuccessful) {

                    seriesList = response.body().results
                    seriesAdapter =
                        SeriesAdapter(seriesList, context!!)

                    onTheAirSeriesRecyclerView.adapter = seriesAdapter
                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getTopRatedSeries() {
        val call: Call<SeriesData> = ApiClient.getClient.getTopRatedSeries()
        call.enqueue(object : Callback<SeriesData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<SeriesData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<SeriesData>?, response: Response<SeriesData>?) =
                if (response!!.isSuccessful) {

                    seriesList = response.body().results
                    seriesAdapter =
                        SeriesAdapter(seriesList, context!!)

                    topRatedSeriesRecyclerView.adapter = seriesAdapter
                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getPopularSeries(view: View) {
        val call: Call<SeriesData> = ApiClient.getClient.getPopularSeries()
        call.enqueue(object : Callback<SeriesData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<SeriesData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<SeriesData>?, response: Response<SeriesData>?) =
                if (response!!.isSuccessful) {

                    bodySeriesData = response.body()
                    imageSlider(bodyMoviesData, bodySeriesData, view)
                    seriesList = response.body().results
                    seriesAdapter =
                        SeriesAdapter(seriesList, context!!)

                    popularSeriesRecyclerView.adapter = seriesAdapter
                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getUpComingMovies() {
        val call: Call<MoviesData> = ApiClient.getClient.getUpComingMovies()
        call.enqueue(object : Callback<MoviesData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<MoviesData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<MoviesData>?, response: Response<MoviesData>?) =
                if (response!!.isSuccessful) {

                    moviesList = response.body().results
                    moviesAdapter =
                        MoviesAdapter(moviesList, context!!)

                    upComingMoviesRecyclerView.adapter = moviesAdapter
                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getTopRatedMovies() {
        val call: Call<MoviesData> = ApiClient.getClient.getTopRatedMovies()
        call.enqueue(object : Callback<MoviesData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<MoviesData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<MoviesData>?, response: Response<MoviesData>?) =
                if (response!!.isSuccessful) {

                    moviesList = response.body().results
                    moviesAdapter =
                        MoviesAdapter(moviesList, context!!)

                    topRatedMoviesRecyclerView.adapter = moviesAdapter
                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getPopularMovies(view: View) {
        val call: Call<MoviesData> = ApiClient.getClient.getPopularMovies()
        call.enqueue(object : Callback<MoviesData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<MoviesData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<MoviesData>?, response: Response<MoviesData>?) =
                if (response!!.isSuccessful) {


                    bodyMoviesData = response.body()
                    imageSlider(bodyMoviesData, bodySeriesData, view)
                    moviesList = response.body().results
                    moviesAdapter =
                        MoviesAdapter(moviesList, context!!)

                    popularMoviesRecyclerView.adapter = moviesAdapter


                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    override fun onClick(v: View?) {
        val sessionId = userPropertiesPreferences.getString("sessionId", "")
        when (v?.id) {

            searchBtn.id -> {
                Navigation.findNavController(v).navigate(R.id.searchFragment)
                accountDetailExpandableLayout.collapse()
            }
            loginBtn.id -> {
                val session = userPropertiesPreferences.getString("sessionId", "")
                if (session.isNullOrEmpty()) {
                    Navigation.findNavController(v).navigate(R.id.loginFragment)
                    accountDetailExpandableLayout.collapse()
                } else{
                    getAccountDetail()
                    loginBtn.setImageResource(R.drawable.user_login)
                }

                if (accountDetailExpandableLayout.isExpanded) {
                    accountDetailExpandableLayout.collapse()

                } else {
                    accountDetailExpandableLayout.expand()
                }
            }
            favoriteListBtn.id -> {
                val vibratorService = context!!.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibratorService.vibrate(100)
                if (sessionId.isNullOrEmpty()) {

                    SimpleTooltip.Builder(context)
                        .anchorView(favoriteListBtn)
                        .text(R.string.login_tooltip)
                        .gravity(Gravity.TOP)
                        .transparentOverlay(true)
                        .overlayWindowBackgroundColor(Color.BLACK)
                        .textColor(resources.getColor(R.color.colorWhite))
                        .arrowColor(resources.getColor(R.color.colorPink))
                        .backgroundColor(resources.getColor(R.color.colorPink))
                        .animated(true)
                        .transparentOverlay(false)
                        .build()
                        .show()
                } else {
                    accountDetailExpandableLayout.collapse()
                    Navigation.findNavController(v).navigate(R.id.favoriteListFragment)
                }


            }
            watchlistBtn.id -> {
                val vibratorService = context!!.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibratorService.vibrate(100)
                if (sessionId.isNullOrEmpty()) {

                    SimpleTooltip.Builder(context)
                        .anchorView(watchlistBtn)
                        .text(R.string.login_tooltip)
                        .gravity(Gravity.TOP)
                        .transparentOverlay(true)
                        .overlayWindowBackgroundColor(Color.BLACK)
                        .textColor(resources.getColor(R.color.colorWhite))
                        .arrowColor(resources.getColor(R.color.colorPink))
                        .animated(true)
                        .backgroundColor(resources.getColor(R.color.colorPink))
                        .transparentOverlay(false)
                        .build()
                        .show()
                } else {
                    accountDetailExpandableLayout.collapse()
                    Navigation.findNavController(v).navigate(R.id.watchlistFragment)
                }
            }
            listMoviesBtn.id -> {
                val vibratorService = context!!.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibratorService.vibrate(100)
                accountDetailExpandableLayout.collapse()
                Navigation.findNavController(v).navigate(R.id.listMoviesByGenreFragment)
            }
            listSeriesBtn.id -> {
                val vibratorService = context!!.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibratorService.vibrate(100)
                accountDetailExpandableLayout.collapse()
                Navigation.findNavController(v).navigate(R.id.listSeriesByGenreFragment)
            }
            logoutBtn.id -> {
                customDialog(v)
            }

        }

    }

    private fun customDialog(view: View) {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.custom_dialog_design, null)
        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mDialogView.dialog_tv.text =resources.getString(R.string.dialog_text_logout)
        mDialogView.ok_btn.setOnClickListener {

            editor.putString("sessionId", "")
            editor.putInt("accountId", 0)
            editor.putString("username", "")
            editor.apply()
            accountDetailExpandableLayout.collapse()
            loginBtn.setImageResource(R.drawable.user)
            mAlertDialog.dismiss()
        }
        mDialogView.cancel_btn.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }


    private fun imageSlider(bodyMoviesData: MoviesData?, bodySeriesData: SeriesData?, view: View) {
        imageList.clear()
        if (bodySeriesData != null && bodyMoviesData != null) {
            for (i in bodyMoviesData.results) {
                val banner = i.backdrop_path
                val title = i.title
                if (banner == null) {
                    imageList.add(SlideModel(R.drawable.default_poster, "$title", true))
                } else {
                    imageList.add(
                        SlideModel(
                            "https://image.tmdb.org/t/p/w500$banner", "$title", false
                        )
                    )
                }

            }
            for (i in bodySeriesData.results) {
                val banner = i.backdrop_path
                val name = i.name
                if (banner == null) {
                    imageList.add(SlideModel(R.drawable.default_poster, "$name", true))
                } else {
                    imageList.add(
                        SlideModel(
                            "https://image.tmdb.org/t/p/w500$banner", "$name", false
                        )
                    )
                }

            }
        }
        imageSlider.setImageList(imageList)
        imageSlider.startSliding(8000)
        Handler().postDelayed({
            blurLayout.visibility = View.GONE
        }, 1000)
        val bundle = Bundle()
        imageSlider.setItemClickListener(object : ItemClickListener {
            override fun onItemSelected(position: Int) {
                if (position < 20) {

                    bundle.putInt("movieId", bodyMoviesData!!.results[position].id)
                    Navigation.findNavController(view).navigate(R.id.movieDetailFragment, bundle)
                } else {
                    var newPosition = position - 20
                    bundle.putInt("seriesId", bodySeriesData!!.results[newPosition].id)
                    Navigation.findNavController(view).navigate(R.id.seriesDetailFragment, bundle)
                }

            }
        })
    }
    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (!Navigation.findNavController(view!!).popBackStack()) {
                  customDialog()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
    private fun customDialog() {
        val mDialogView = LayoutInflater.from(context!!).inflate(R.layout.custom_dialog_design, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(context!!).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mDialogView.dialog_tv.text = resources.getString(R.string.dialog_text_exit)
        mDialogView.ok_btn.setOnClickListener {

            activity!!.finish()
        }
        mDialogView.cancel_btn.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }
    private fun getAccountDetail() {
        usernameTxt.text = userPropertiesPreferences.getString(("username"), "")
    }
}