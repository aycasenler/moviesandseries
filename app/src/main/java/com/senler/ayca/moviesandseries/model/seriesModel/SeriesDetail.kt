package com.senler.ayca.moviesandseries.model.seriesModel

import com.senler.ayca.moviesandseries.model.ProductionCompanies
import com.senler.ayca.moviesandseries.model.movieModel.GenresList
import com.google.gson.annotations.SerializedName

data class SeriesDetail(
    @SerializedName("backdrop_path")
    val backdrop_path: String,
    @SerializedName("created_by")
    val created_by: List<CreatedByList>,
    @SerializedName("first_air_date")
    val first_air_date: String,
    @SerializedName("genres")
    val genres: List<GenresList>,
    @SerializedName("homepage")
    val homepage: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("last_air_date")
    val last_air_date: String,
    @SerializedName("networks")
    val networks: List<Networks>,
    @SerializedName("number_of_seasons")
    val number_of_seasons: Int,
    @SerializedName("number_of_episodes")
    val number_of_episodes: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("poster_path")
    val poster_path: String,
    @SerializedName("production_companies")
    val production_companies: List<ProductionCompanies>,
    @SerializedName("seasons")
    val seasons: List<Seasons>,
    @SerializedName("status")
    val status: String,
    @SerializedName("vote_average")
    val vote_average: Double


)