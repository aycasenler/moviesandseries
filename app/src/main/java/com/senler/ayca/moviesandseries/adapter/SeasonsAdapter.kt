package com.senler.ayca.moviesandseries.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.seriesModel.Seasons
import com.senler.ayca.moviesandseries.model.seriesModel.SeasonsDetail
import com.senler.ayca.moviesandseries.retrofit.ApiClient
import com.squareup.picasso.Picasso
import net.cachapa.expandablelayout.ExpandableLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SeasonsAdapter (private var seasonsList: List<Seasons>, private var context: Context, private var seriesId: Int) :
    RecyclerView.Adapter<SeasonsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.seasons_list_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return seasonsList.size
    }

    override fun onBindViewHolder(holder: SeasonsAdapter.ViewHolder, position: Int) {

        val seasons = seasonsList[position]

        val url = "https://image.tmdb.org/t/p/w500" + seasons.poster_path

        if (seasons.poster_path.isNullOrEmpty())
            holder.posterPathImg.setImageResource(R.drawable.default_poster)
        else
            picassoImage(holder.posterPathImg, url)

        holder.nameTv.text = seasons.name
        holder.overviewTv.text = seasons.overview
        holder.airDateTv.text = seasons.air_date
        holder.episodeCountTv.text = seasons.episode_count.toString()
        holder.seasonNumberTv.text = seasons.season_number.toString()

        holder.itemView.setOnClickListener {
            clickSeasons(position,holder)
        }

    }

    private fun clickSeasons(position: Int,holder: ViewHolder) {
        getEpisodeDetail(holder.episodeRecyclerView, seasonsList[position].season_number)
        if (holder.seasonsExpandable.isExpanded) {
            holder.seasonsExpandable.collapse()

        } else {
            holder.seasonsExpandable.expand()
        }

    }

    private fun getEpisodeDetail(episodeRecyclerView: RecyclerView, seasonNumber: Int) {
        val call: Call<SeasonsDetail> = ApiClient.getClient.getEpisodeDetail(seriesId, seasonNumber)
        call.enqueue(object : Callback<SeasonsDetail> {
            override fun onFailure(call: Call<SeasonsDetail>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<SeasonsDetail>?, response: Response<SeasonsDetail>?) =
                if (response!!.isSuccessful) {
                    val episodeAdapter =
                        EpisodeAdapter(response.body().episodes,context, seriesId)

                    episodeRecyclerView.adapter = episodeAdapter

                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var posterPathImg: ImageView = itemLayoutView.findViewById(R.id.poster_path_img)
        var nameTv: TextView = itemLayoutView.findViewById(R.id.name_tv)
        var overviewTv: TextView = itemLayoutView.findViewById(R.id.overview_tv)
        var seasonNumberTv: TextView = itemLayoutView.findViewById(R.id.season_number_tv)
        var episodeCountTv: TextView = itemLayoutView.findViewById(R.id.episode_count_tv)
        var airDateTv: TextView = itemLayoutView.findViewById(R.id.air_date_tv)
        var seasonsExpandable: ExpandableLayout = itemLayoutView.findViewById(R.id.seasons_expandable_view)
        var episodeRecyclerView: RecyclerView = itemLayoutView.findViewById(R.id.episode_recycler_view)

        init {
            episodeRecyclerView.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }
    }

    fun picassoImage(imageView: ImageView, url: String) {
        Picasso.get().load(url).into(imageView)
    }
}