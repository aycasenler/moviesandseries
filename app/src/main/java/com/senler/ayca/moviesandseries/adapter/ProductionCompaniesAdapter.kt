package com.senler.ayca.moviesandseries.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.ProductionCompanies
import com.squareup.picasso.Picasso

class ProductionCompaniesAdapter(
    private var companiesList: List<ProductionCompanies>,
    private var context: Context
) :
    RecyclerView.Adapter<ProductionCompaniesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.production_companies_list_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return companiesList.size
    }

    override fun onBindViewHolder(holder: ProductionCompaniesAdapter.ViewHolder, position: Int) {

        val companies = companiesList[position]

        if (companies.logo_path.isNullOrEmpty()) {
//            holder.companiesImg.visibility = View.GONE
            holder.productionCompaniesLL.layoutParams.width = 0
            holder.productionCompaniesLL.layoutParams.height = 0


        } else {
            val url =
                "https://image.tmdb.org/t/p/w500" + companies.logo_path
            picassoImage(holder.companiesImg, url)
            holder.productionCompaniesLL.layoutParams.width = 370
            holder.productionCompaniesLL.layoutParams.height = 400

        }

    }


    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var companiesImg: ImageView = itemLayoutView.findViewById(R.id.production_companies_img)
        var productionCompaniesLL : LinearLayout = itemLayoutView.findViewById(R.id.production_companies_ll)

    }

    fun picassoImage(imageView: ImageView, url: String) {
        Picasso.get().load(url).resize(250,250).into(imageView)
    }
}