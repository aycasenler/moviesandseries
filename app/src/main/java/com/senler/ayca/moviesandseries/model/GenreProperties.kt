package com.senler.ayca.moviesandseries.model

import com.google.gson.annotations.SerializedName

data class GenreProperties(
    @SerializedName("id")
    val id : Int,
    @SerializedName("name")
    val name: String
)