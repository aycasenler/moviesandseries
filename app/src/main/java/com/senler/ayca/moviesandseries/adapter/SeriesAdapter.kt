package com.senler.ayca.moviesandseries.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesProperties
import com.squareup.picasso.Picasso
import hyogeun.github.com.colorratingbarlib.ColorRatingBar

class SeriesAdapter(private var seriesList: List<SeriesProperties>, private var context: Context) :
    RecyclerView.Adapter<SeriesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.home_screen_list_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return seriesList.size
    }

    override fun onBindViewHolder(holder: SeriesAdapter.ViewHolder, position: Int) {

        val series = seriesList[position]

        val url = "https://image.tmdb.org/t/p/w500" + series.poster_path

        if (series.poster_path.isNullOrEmpty())
            holder.seriesImg.setImageResource(R.drawable.default_poster)
        else
            picassoImage(holder.seriesImg, url)

        holder.seriesTitle.text = series.name
        holder.ratingBar.rating = ((series.vote_average) / 2).toFloat()
 //       holder.seriesVote.text = series.vote_average.toString()

        holder.itemView.setOnClickListener {
            clickSeries(position, it)
        }

    }

    private fun clickSeries(position: Int, view: View) {
        val bundle = Bundle()
        bundle.putInt("seriesId", seriesList[position].id)
        Navigation.findNavController(view).navigate(R.id.seriesDetailFragment, bundle)
    }

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var seriesImg: ImageView = itemLayoutView.findViewById(R.id.show_poster_img)
        var seriesTitle: TextView = itemLayoutView.findViewById(R.id.show_title_tv)
       // var seriesVote: TextView = itemLayoutView.findViewById(R.id.show_vote_average_tv)
       var ratingBar: ColorRatingBar = itemLayoutView.findViewById(R.id.rating_bar)
    }

    fun picassoImage(imageView: ImageView, url: String) {
        Picasso.get().load(url).resize(150, 250).into(imageView)
    }
}