package com.senler.ayca.moviesandseries.model

import com.google.gson.annotations.SerializedName

data class VideoProperties(
    @SerializedName("key")
    val key : String
)