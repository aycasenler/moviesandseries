package com.senler.ayca.moviesandseries.model.review

import com.google.gson.annotations.SerializedName

data class Review(
    @SerializedName("id")
    val id: Int,
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: List<ReviewProperties>
)