package com.senler.ayca.moviesandseries.model.castModel

import com.google.gson.annotations.SerializedName

data class CastSeriesData(
    @SerializedName("cast")
    val cast : List<CastSeriesProperties>
)