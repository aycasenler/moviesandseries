package com.senler.ayca.moviesandseries.model.castModel

import com.google.gson.annotations.SerializedName

data class PersonalInformations(
    @SerializedName("birthday")
    val birthday: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("deathday")
    val deathday: String,
    @SerializedName("biography")
    val biography: String,
    @SerializedName("place_of_birth")
    val place_of_birth: String,
    @SerializedName("profile_path")
    val profile_path: String
)