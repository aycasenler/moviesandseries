package com.senler.ayca.moviesandseries.model.castModel

import com.google.gson.annotations.SerializedName

data class CastMoviesProperties(
    @SerializedName("title")
    val title: String,
    @SerializedName("poster_path")
    val poster_path: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("vote_average")
    val vote_average: Double
)