package com.senler.ayca.moviesandseries.fragment


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.Account
import com.senler.ayca.moviesandseries.model.Session
import com.senler.ayca.moviesandseries.model.Token
import com.senler.ayca.moviesandseries.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginScreenFragment : Fragment(), View.OnClickListener {
    lateinit var signUpTxt: TextView
    lateinit var loginBtn: Button
    lateinit var usernameEt: EditText
    lateinit var passwordEt: EditText

    lateinit var userPropertiesPreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    private var requestToken: String = ""
    private var validateWithLogin: String = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.login_screen_fragment, container, false)
        initView(view)
        backPressHandler()
        return view
    }

    private fun initView(view: View) {

        signUpTxt = view.findViewById(R.id.sign_up_txt)
        loginBtn = view.findViewById(R.id.login_btn)
        usernameEt = view.findViewById(R.id.username_et)
        passwordEt = view.findViewById(R.id.password_et)
        userPropertiesPreferences =
            context!!.getSharedPreferences("userProperties", Context.MODE_PRIVATE)
        editor = userPropertiesPreferences.edit()

        getToken()
        signUpTxt.setOnClickListener(this)
        loginBtn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            signUpTxt.id -> {
                signUp()
            }
            loginBtn.id -> {

                val username = usernameEt.text.toString()
                val password = passwordEt.text.toString()

                if (!requestToken.isNullOrEmpty())
                    validateWithLogin(username, password, requestToken)

            }
        }
    }

    private fun createSession(validateWithLogin: String) {
        val call: Call<Session> = ApiClient.getClient.getSession(validateWithLogin)
        call.enqueue(object : Callback<Session> {
            override fun onFailure(call: Call<Session>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<Session>?, response: Response<Session>?) =
                if (response!!.isSuccessful) {
                    if (response.body().success) {

                        editor.putString("sessionId", response.body().session_id)
                        editor.apply()

                        Navigation.findNavController(view!!).navigate(R.id.homeScreenFragment)
                        getAccountDetail()
                    } else {
                        Toast.makeText(context, R.string.fail_login, Toast.LENGTH_SHORT)
                            .show()
                    }


                } else {
                    Toast.makeText(context, R.string.fail_login, Toast.LENGTH_SHORT)
                        .show()
                }
        }
        )
    }

    private fun getAccountDetail() {
        val session = userPropertiesPreferences.getString("sessionId", "")
        val call: Call<Account> = ApiClient.getClient.getAccountDetail(session!!)
        call.enqueue(object : Callback<Account> {
            override fun onFailure(call: Call<Account>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<Account>?, response: Response<Account>?) =
                if (response!!.isSuccessful) {
                    editor.putInt("accountId", response.body().id)
                    editor.putString("username", response.body().username)
                    editor.apply()

                } else {
                }
        }
        )
    }

    private fun getToken() {
        val call: Call<Token> = ApiClient.getClient.getToken()
        call.enqueue(object : Callback<Token> {
            override fun onFailure(call: Call<Token>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<Token>?, response: Response<Token>?) =
                if (response!!.isSuccessful) {
                    if (response.body().success) {
                        requestToken = response.body().request_token

                    } else {

                    }


                } else {
                }
        }
        )
    }

    private fun validateWithLogin(username: String, password: String, requestToken: String) {
        val call: Call<Token> =
            ApiClient.getClient.createSessionWithLogin(username, password, requestToken)
        call.enqueue(object : Callback<Token> {
            override fun onFailure(call: Call<Token>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<Token>?, response: Response<Token>?) =
                if (response!!.isSuccessful) {
                    if (response.body().success) {
                        validateWithLogin = response.body().request_token
                        createSession(validateWithLogin)
                    } else {
                        Toast.makeText(context, R.string.fail_login, Toast.LENGTH_SHORT)
                            .show()
                    }

                } else {
                    Toast.makeText(context, R.string.fail_login, Toast.LENGTH_SHORT)
                        .show()
                }
        }
        )
    }


    private fun signUp() {
        val call: Call<Token> = ApiClient.getClient.getToken()
        call.enqueue(object : Callback<Token> {
            override fun onFailure(call: Call<Token>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<Token>?, response: Response<Token>?) =
                if (response!!.isSuccessful) {

                    val token = response.body().request_token
                    val bundle = Bundle()
                    bundle.putString("request_token", token)

                    val uri = Uri.parse("https://www.themoviedb.org/authenticate/$token")
                    val intent = Intent(Intent.ACTION_VIEW, uri)
                    startActivity(intent)

                } else {
                }

        }
        )
    }

    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view!!).popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

}