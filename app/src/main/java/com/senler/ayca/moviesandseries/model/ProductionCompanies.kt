package com.senler.ayca.moviesandseries.model

import com.google.gson.annotations.SerializedName

data class ProductionCompanies(
    @SerializedName("logo_path")
    val logo_path : String,
    @SerializedName("name")
    val name : String,
    @SerializedName("origin_country")
    val origin_country : String
)