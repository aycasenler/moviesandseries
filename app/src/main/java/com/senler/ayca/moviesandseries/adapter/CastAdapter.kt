package com.senler.ayca.moviesandseries.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.castModel.CastProperties
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class CastAdapter(
    private var castList: List<CastProperties>,
    private var context: Context
) :
    RecyclerView.Adapter<CastAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.cast_list_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return castList.size
    }

    override fun onBindViewHolder(holder: CastAdapter.ViewHolder, position: Int) {

        val cast = castList[position]

        if (cast.profile_path.isNullOrEmpty()) {

            holder.castNameTv.width = 0
            holder.castLL.layoutParams.width = 0
            holder.castLL.layoutParams.height = 0
        } else {
            val url =
                "https://image.tmdb.org/t/p/w500" + cast.profile_path
            holder.castNameTv.width = 350
            picassoImage(holder.castImg, url)
            holder.castNameTv.text = cast.name
            holder.castLL.layoutParams.width = 350
            holder.castLL.layoutParams.height = 450

        }


        holder.itemView.setOnClickListener {
            clickCast(position, it)
        }

    }

    private fun clickCast(position: Int, view: View) {
        val bundle = Bundle()
        bundle.putInt("personId", castList[position].id)
        Navigation.findNavController(view).navigate(R.id.personDetailFragment, bundle)
    }

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var castImg: ImageView = itemLayoutView.findViewById(R.id.cast_img)
        var castNameTv: TextView = itemLayoutView.findViewById(R.id.cast_name_tv)
        var castLL: LinearLayout = itemLayoutView.findViewById(R.id.cast_ll)


    }

    fun picassoImage(imageView: ImageView, url: String) {
        Picasso.get().load(url).transform(CropCircleTransformation()).into(imageView)
    }
}