package com.senler.ayca.moviesandseries.model.seriesModel

import com.google.gson.annotations.SerializedName

data class Episodes(
    @SerializedName("episode_number")
    val episode_number: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("season_number")
    val season_number: Int,
    @SerializedName("id")
    val id: Int
)