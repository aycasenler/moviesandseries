package com.senler.ayca.moviesandseries.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.adapter.MoviesAdapter
import com.senler.ayca.moviesandseries.adapter.SeriesAdapter
import com.senler.ayca.moviesandseries.model.movieModel.MoviesData
import com.senler.ayca.moviesandseries.model.movieModel.MoviesProperties
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesData
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesProperties
import com.senler.ayca.moviesandseries.retrofit.ApiClient
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ChasingDots
import com.github.ybq.android.spinkit.style.CubeGrid
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FavoriteListFragment : Fragment(), View.OnClickListener {
    lateinit var favoriteListRecyclerView: RecyclerView
    lateinit var userPropertiesPreferences: SharedPreferences
    lateinit var favoriteMoviesAdapter: MoviesAdapter
    lateinit var favoriteMoviesList: List<MoviesProperties>
    lateinit var favoriteSeriesAdapter: SeriesAdapter
    lateinit var favoriteSeriesList: List<SeriesProperties>
    lateinit var moviesBtn: Button
    lateinit var seriesBtn: Button
    lateinit var moviesView: View
    lateinit var seriesView: View
    lateinit var progressBar: ProgressBar
    lateinit var blurLayout: LinearLayout
    lateinit var backBtn: ImageButton
    lateinit var adView: AdView

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.favorite_list_fragment, container, false)
        activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        initView(view)
        initAd()
        backPressHandler()
        return view
    }

    private fun initAd() {
        MobileAds.initialize(context, "ca-app-pub-1898235865477290~9103030179")
        val adRequest = AdRequest.Builder().build()
        adView?.loadAd(adRequest)
    }

    private fun initView(view: View) {
        adView = view.findViewById(R.id.ad_banner_favorite_list_fragment)
        moviesBtn = view.findViewById(R.id.movies_btn)
        seriesBtn = view.findViewById(R.id.series_btn)
        moviesView = view.findViewById(R.id.movies_view)
        seriesView = view.findViewById(R.id.series_view)
        favoriteMoviesList = mutableListOf()
        favoriteSeriesList = mutableListOf()
        favoriteListRecyclerView = view.findViewById(R.id.favorite_list_recycler_view)
        favoriteListRecyclerView.layoutManager =
            GridLayoutManager(context, 3)

        userPropertiesPreferences =
            context!!.getSharedPreferences("userProperties", Context.MODE_PRIVATE)

        progressBar = view.findViewById(R.id.spin_kit)
        blurLayout = view.findViewById(R.id.blur_layout)
        val cubeGrid: Sprite = CubeGrid()
        progressBar.setIndeterminateDrawableTiled(cubeGrid)

        getFavoriteMoviesList()

        backBtn = view.findViewById(R.id.back_btn)

        moviesBtn.setOnClickListener(this)
        seriesBtn.setOnClickListener(this)
        backBtn.setOnClickListener(this)
    }

    private fun getFavoriteMoviesList() {
        val sessionId = userPropertiesPreferences.getString("sessionId", "")
        val accountId = userPropertiesPreferences.getInt("accountId", 0)
        val call: Call<MoviesData> = ApiClient.getClient.getFavoriteMovies(accountId, sessionId!!)
        call.enqueue(object : Callback<MoviesData> {
            override fun onFailure(call: Call<MoviesData>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<MoviesData>?, response: Response<MoviesData>?) =
                if (response!!.isSuccessful) {
                    blurLayout.visibility = View.GONE
                    favoriteMoviesList = response.body().results
                    favoriteMoviesAdapter =
                        MoviesAdapter(favoriteMoviesList, context!!)

                    favoriteListRecyclerView.adapter = favoriteMoviesAdapter

                } else {
                }
        }
        )
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            moviesBtn.id -> {
                moviesView.visibility = View.VISIBLE
                seriesView.visibility = View.INVISIBLE
                getFavoriteMoviesList()
                blurLayout.visibility = View.VISIBLE
            }
            seriesBtn.id -> {
                moviesView.visibility = View.INVISIBLE
                seriesView.visibility = View.VISIBLE
                getFavoriteSeriesList()
                blurLayout.visibility = View.VISIBLE
            }

            backBtn.id -> {
                Navigation.findNavController(v).navigate(R.id.homeScreenFragment)
            }
        }
    }

    private fun getFavoriteSeriesList() {
        val sessionId = userPropertiesPreferences.getString("sessionId", "")
        val accountId = userPropertiesPreferences.getInt("accountId", 0)
        val call: Call<SeriesData> = ApiClient.getClient.getFavoriteSeries(accountId, sessionId!!)
        call.enqueue(object : Callback<SeriesData> {
            override fun onFailure(call: Call<SeriesData>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<SeriesData>?, response: Response<SeriesData>?) =
                if (response!!.isSuccessful) {
                    blurLayout.visibility = View.GONE
                    favoriteSeriesList = response.body().results
                    favoriteSeriesAdapter =
                        SeriesAdapter(favoriteSeriesList, context!!)

                    favoriteListRecyclerView.adapter = favoriteSeriesAdapter

                } else {
                }
        }
        )
    }

    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view!!).popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}