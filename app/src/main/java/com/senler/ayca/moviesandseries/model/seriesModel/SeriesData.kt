package com.senler.ayca.moviesandseries.model.seriesModel

import com.google.gson.annotations.SerializedName

data class SeriesData(
    @SerializedName("results")
    val results :List<SeriesProperties>
)