package com.senler.ayca.moviesandseries.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.movieModel.MoviesProperties
import com.squareup.picasso.Picasso
import hyogeun.github.com.colorratingbarlib.ColorRatingBar

class MoviesAdapter(private var moviesList: List<MoviesProperties>, private var context: Context) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.home_screen_list_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return moviesList.size
    }

    override fun onBindViewHolder(holder: MoviesAdapter.ViewHolder, position: Int) {

        val movies = moviesList[position]

        val url = "https://image.tmdb.org/t/p/w500" + movies.poster_path

        if (movies.poster_path.isNullOrEmpty())
            holder.movieImg.setImageResource(R.drawable.default_poster)
        else
            picassoImage(holder.movieImg, url)

        holder.movieTitle.text = movies.title
        holder.ratingBar.rating = ((movies.vote_average) / 2).toFloat()
        holder.itemView.setOnClickListener {
            clickMovies(position, it)
        }
    }

    private fun clickMovies(position: Int, view: View) {
        val bundle = Bundle()
        bundle.putInt("movieId", moviesList[position].id)
        Navigation.findNavController(view).navigate(R.id.movieDetailFragment, bundle)
    }


    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var movieImg: ImageView = itemLayoutView.findViewById(R.id.show_poster_img)
        var movieTitle: TextView = itemLayoutView.findViewById(R.id.show_title_tv)
        var ratingBar: ColorRatingBar = itemLayoutView.findViewById(R.id.rating_bar)
    }

    fun picassoImage(imageView: ImageView, url: String) {
        Picasso.get().load(url).resize(150, 250).into(imageView)
    }
}