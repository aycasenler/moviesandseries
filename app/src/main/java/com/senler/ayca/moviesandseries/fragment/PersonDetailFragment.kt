package com.senler.ayca.moviesandseries.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.borjabravo.readmoretextview.ReadMoreTextView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.adapter.CastMoviesAdapter
import com.senler.ayca.moviesandseries.adapter.CastSeriesAdapter
import com.senler.ayca.moviesandseries.model.castModel.*
import com.senler.ayca.moviesandseries.retrofit.ApiClient
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.CubeGrid
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PersonDetailFragment : Fragment(), View.OnClickListener {
    lateinit var personImg: ImageView
    lateinit var birthdayTv: TextView
    lateinit var deathDayTv: TextView
    lateinit var nameTv: TextView
    lateinit var biographyTv: ReadMoreTextView
    lateinit var placeOfBirthTv: TextView
    lateinit var castMoviesRecyclerView: RecyclerView
    lateinit var castMoviesAdapter: CastMoviesAdapter
    lateinit var castMoviesList: List<CastMoviesProperties>
    lateinit var castSeriesRecyclerView: RecyclerView
    lateinit var castSeriesAdapter : CastSeriesAdapter
    lateinit var castSeriesList : List<CastSeriesProperties>
    lateinit var backBtn  : ImageButton
    lateinit var progressBar : ProgressBar
    lateinit var blurLayout : LinearLayout
    lateinit var adView: AdView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.person_detail_fragment, container, false)
        initView(view)
        initAd()
        backPressHandler()
        return view
    }
    private fun initAd() {
        MobileAds.initialize(context, "ca-app-pub-1898235865477290~9103030179")
        val adRequest = AdRequest.Builder().build()
        adView?.loadAd(adRequest)

    }
    private fun initView(view: View) {
        adView = view.findViewById(R.id.ad_banner_person_detail_fragment)
        personImg = view.findViewById(R.id.person_img)
        birthdayTv = view.findViewById(R.id.birthday_tv)
        deathDayTv = view.findViewById(R.id.deathday_tv)
        nameTv = view.findViewById(R.id.name_tv)
        placeOfBirthTv = view.findViewById(R.id.place_of_birth_tv)
        biographyTv = view.findViewById(R.id.biography_tv)

        castSeriesRecyclerView = view.findViewById(R.id.cast_series_recycler_view)
        castSeriesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        castMoviesRecyclerView = view.findViewById(R.id.cast_movies_recycler_view)
        castMoviesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        castMoviesList = mutableListOf()
        castSeriesList = mutableListOf()
        backBtn = view.findViewById(R.id.back_btn)

        progressBar = view.findViewById(R.id.spin_kit)
        blurLayout = view.findViewById(R.id.blur_layout)
        val cubeGrid: Sprite = CubeGrid()
        progressBar.setIndeterminateDrawableTiled(cubeGrid)

        val personId = this.arguments?.getInt("personId")
        getPersonalInformations(personId!!)
        getCastMovies(personId)
        getCastSeries(personId)



        backBtn.setOnClickListener(this)
    }

    private fun getCastSeries(personId: Int) {
        val call: Call<CastSeriesData> = ApiClient.getClient.getCastSeries(personId)
        call.enqueue(object : Callback<CastSeriesData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<CastSeriesData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(
                call: Call<CastSeriesData>?,
                response: Response<CastSeriesData>?
            ) =
                if (response!!.isSuccessful) {
                    castSeriesList = response.body().cast
                    castSeriesAdapter = CastSeriesAdapter(castSeriesList, context!!)
                    castSeriesRecyclerView.adapter = castSeriesAdapter
                    blurLayout.visibility = View.GONE

                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getCastMovies(personId: Int) {
        val call: Call<CastMoviesData> = ApiClient.getClient.getCastMovies(personId)
        call.enqueue(object : Callback<CastMoviesData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<CastMoviesData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(
                call: Call<CastMoviesData>?,
                response: Response<CastMoviesData>?
            ) =
                if (response!!.isSuccessful) {
                    castMoviesList = response.body().cast
                    castMoviesAdapter = CastMoviesAdapter(castMoviesList, context!!)
                    castMoviesRecyclerView.adapter = castMoviesAdapter

                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getPersonalInformations(personId: Int) {
        val call: Call<PersonalInformations> = ApiClient.getClient.getPersonalInformations(personId)
        call.enqueue(object : Callback<PersonalInformations> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<PersonalInformations>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(
                call: Call<PersonalInformations>?,
                response: Response<PersonalInformations>?
            ) =
                if (response!!.isSuccessful) {

                    birthdayTv.text = response.body().birthday
                    nameTv.text = response.body().name
                    placeOfBirthTv.text = response.body().place_of_birth

                    biographyTv.text = response.body().biography


                    if (response.body().deathday.isNullOrEmpty()) {
                        deathDayTv.visibility = View.GONE
                    } else {
                        deathDayTv.text = response.body().deathday
                    }
                    if (response.body().profile_path.isNullOrEmpty()) {
                        personImg.visibility = View.GONE

                    } else {
                        val url =
                            "https://image.tmdb.org/t/p/w500" + response.body().profile_path
                        picassoImage(personImg, url)
                    }
                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    fun picassoImage(imageView: ImageView, url: String) {
        Picasso.get().load(url).into(imageView)
    }
    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view!!).popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
    override fun onClick(v: View?) {
        Navigation.findNavController(v!!).popBackStack()
    }
}