package com.senler.ayca.moviesandseries.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.SearchResults
import com.squareup.picasso.Picasso

class SearchShowsAdapter(
    private var searchList: List<SearchResults>,
    private var context: Context,
    private var isMovie: Boolean
) :
    RecyclerView.Adapter<SearchShowsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.search_shows_list_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return searchList.size
    }

    override fun onBindViewHolder(holder: SearchShowsAdapter.ViewHolder, position: Int) {

        val search = searchList[position]

        val url = "https://image.tmdb.org/t/p/w500" + search.poster_path
        picassoImage(holder.showImg, url)

        if (isMovie) {
            holder.showTitle.text = search.title
        } else {
            holder.showTitle.text = search.name
        }

        holder.itemView.setOnClickListener {
            clickShows(position, it)
        }
    }

    private fun clickShows(position: Int, view: View) {
        val bundle = Bundle()
        if (isMovie) {
            bundle.putInt("movieId", searchList[position].id)
            Navigation.findNavController(view).navigate(R.id.movieDetailFragment, bundle)
        } else {
            bundle.putInt("seriesId", searchList[position].id)
            Navigation.findNavController(view).navigate(R.id.seriesDetailFragment, bundle)
        }


    }

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var showImg: ImageView = itemLayoutView.findViewById(R.id.show_poster_img)
        var showTitle: TextView = itemLayoutView.findViewById(R.id.show_title_tv)

    }

    fun picassoImage(imageView: ImageView, url: String) {
        Picasso.get().load(url).resize(150, 250).into(imageView)
    }
}