package com.senler.ayca.moviesandseries.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.borjabravo.readmoretextview.ReadMoreTextView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.model.review.ReviewProperties


class ReviewAdapter(
    private var reviewList: List<ReviewProperties>,
    private var context: Context
) :
    RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.review_list_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return reviewList.size
    }

    override fun onBindViewHolder(holder: ReviewAdapter.ViewHolder, position: Int) {

        val reviews = reviewList[position]
        holder.authorTv.text = reviews.author
        holder.contentTv.text = reviews.content


    }


    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var authorTv: TextView = itemLayoutView.findViewById(R.id.author_tv)
        var contentTv: ReadMoreTextView = itemLayoutView.findViewById(R.id.content_tv)


    }


}