package com.senler.ayca.moviesandseries.model

import com.google.gson.annotations.SerializedName

data class SearchResults(
    @SerializedName("known_for_department")
    val known_for_department: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("media_type")
    val media_type: String,
    @SerializedName("profile_path")
    val profile_path: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("poster_path")
    val poster_path : String

)