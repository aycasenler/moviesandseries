package com.senler.ayca.moviesandseries.model.seriesModel

import com.google.gson.annotations.SerializedName

data class Networks(
    @SerializedName("name")
    val name :String,
    @SerializedName("logo_path")
    val logo_path :String
)