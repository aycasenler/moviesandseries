package com.senler.ayca.moviesandseries.model

import com.google.gson.annotations.SerializedName

data class VideoData(
    @SerializedName("results")
    val results : List<VideoProperties>
)