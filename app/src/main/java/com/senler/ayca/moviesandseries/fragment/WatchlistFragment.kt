package com.senler.ayca.moviesandseries.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.adapter.MoviesAdapter
import com.senler.ayca.moviesandseries.adapter.SeriesAdapter
import com.senler.ayca.moviesandseries.model.movieModel.MoviesData
import com.senler.ayca.moviesandseries.model.movieModel.MoviesProperties
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesData
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesProperties
import com.senler.ayca.moviesandseries.retrofit.ApiClient
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ChasingDots
import com.github.ybq.android.spinkit.style.CubeGrid
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WatchlistFragment : Fragment(), View.OnClickListener {
    lateinit var watchlistRecyclerView: RecyclerView
    lateinit var userPropertiesPreferences: SharedPreferences
    lateinit var movieWatchlistAdapter: MoviesAdapter
    lateinit var movieWatchlist: List<MoviesProperties>
    lateinit var seriesWatchlistAdapter: SeriesAdapter
    lateinit var seriesWatchlist: List<SeriesProperties>
    lateinit var moviesBtn: Button
    lateinit var seriesBtn: Button
    lateinit var moviesView: View
    lateinit var seriesView: View
    lateinit var progressBar : ProgressBar
    lateinit var blurLayout : LinearLayout
    lateinit var backBtn: ImageButton
    lateinit var adView: AdView
    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.watchlist_fragment, container, false)
        initView(view)
        initAd()
        backPressHandler()
        activity!!.requestedOrientation =ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        return view
    }
    private fun initAd() {
        MobileAds.initialize(context, "ca-app-pub-1898235865477290~9103030179")
        val adRequest = AdRequest.Builder().build()
        adView?.loadAd(adRequest)

    }
    private fun initView(view: View) {
        adView = view.findViewById(R.id.ad_banner_watchlist_fragment)
        moviesBtn = view.findViewById(R.id.movies_btn)
        seriesBtn = view.findViewById(R.id.series_btn)
        moviesView = view.findViewById(R.id.movies_view)
        seriesView = view.findViewById(R.id.series_view)
        movieWatchlist = mutableListOf()
        seriesWatchlist = mutableListOf()
        watchlistRecyclerView = view.findViewById(R.id.watchlist_recycler_view)
        watchlistRecyclerView.layoutManager =
            GridLayoutManager(context, 3)

        userPropertiesPreferences =
            context!!.getSharedPreferences("userProperties", Context.MODE_PRIVATE)
        progressBar = view.findViewById(R.id.spin_kit)
        progressBar = view.findViewById(R.id.spin_kit)
        blurLayout = view.findViewById(R.id.blur_layout)

        val cubeGrid: Sprite = CubeGrid()
        progressBar.setIndeterminateDrawableTiled(cubeGrid)
        getMovieWatchlist()

        backBtn = view.findViewById(R.id.back_btn)

        moviesBtn.setOnClickListener(this)
        seriesBtn.setOnClickListener(this)
        backBtn.setOnClickListener(this)
    }

    private fun getMovieWatchlist() {
        val sessionId = userPropertiesPreferences.getString("sessionId", "")
        val accountId = userPropertiesPreferences.getInt("accountId", 0)
        val call: Call<MoviesData> = ApiClient.getClient.getMovieWatchlist(accountId, sessionId!!)
        call.enqueue(object : Callback<MoviesData> {
            override fun onFailure(call: Call<MoviesData>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<MoviesData>?, response: Response<MoviesData>?) =
                if (response!!.isSuccessful) {
                    blurLayout.visibility = View.GONE
                    movieWatchlist = response.body().results
                    movieWatchlistAdapter =
                        MoviesAdapter(movieWatchlist, context!!)

                    watchlistRecyclerView.adapter = movieWatchlistAdapter

                } else {
                }
        }
        )
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            moviesBtn.id -> {
                moviesView.visibility = View.VISIBLE
                seriesView.visibility = View.INVISIBLE
                getMovieWatchlist()
                blurLayout.visibility = View.VISIBLE
            }
            seriesBtn.id -> {
                moviesView.visibility = View.INVISIBLE
                seriesView.visibility = View.VISIBLE
                getSeriesWatchlist()
                blurLayout.visibility = View.VISIBLE
            }
            backBtn.id->{
                Navigation.findNavController(v).navigate(R.id.homeScreenFragment)
            }

        }
    }

    private fun getSeriesWatchlist() {
        val sessionId = userPropertiesPreferences.getString("sessionId", "")
        val accountId = userPropertiesPreferences.getInt("accountId", 0)
        val call: Call<SeriesData> = ApiClient.getClient.getSeriesWatchlist(accountId, sessionId!!)
        call.enqueue(object : Callback<SeriesData> {
            override fun onFailure(call: Call<SeriesData>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<SeriesData>?, response: Response<SeriesData>?) =
                if (response!!.isSuccessful) {
                    blurLayout.visibility = View.GONE
                    seriesWatchlist = response.body().results
                    seriesWatchlistAdapter =
                        SeriesAdapter(seriesWatchlist, context!!)

                    watchlistRecyclerView.adapter = seriesWatchlistAdapter

                } else {
                }
        }
        )
    }
    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view!!).popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}