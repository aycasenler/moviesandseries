package com.senler.ayca.moviesandseries.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.borjabravo.readmoretextview.ReadMoreTextView
import com.senler.ayca.moviesandseries.R
import com.senler.ayca.moviesandseries.adapter.CastAdapter
import com.senler.ayca.moviesandseries.adapter.ProductionCompaniesAdapter
import com.senler.ayca.moviesandseries.adapter.ReviewAdapter
import com.senler.ayca.moviesandseries.adapter.SeasonsAdapter
import com.senler.ayca.moviesandseries.model.*
import com.senler.ayca.moviesandseries.model.castModel.CastData
import com.senler.ayca.moviesandseries.model.castModel.CastProperties
import com.senler.ayca.moviesandseries.model.review.Review
import com.senler.ayca.moviesandseries.model.review.ReviewProperties
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesData
import com.senler.ayca.moviesandseries.model.seriesModel.SeriesDetail
import com.senler.ayca.moviesandseries.retrofit.ApiClient
import com.github.florent37.viewtooltip.ViewTooltip
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ChasingDots
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.squareup.picasso.Picasso
import net.cachapa.expandablelayout.ExpandableLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SeriesDetailFragment : Fragment(), View.OnClickListener {
    lateinit var backdropPathImg: ImageView
    lateinit var posterPathImg: ImageView
    lateinit var productionCompaniesTv: TextView
    lateinit var titleTv: TextView
    lateinit var genresTv: TextView
    lateinit var firstAirDateTv: TextView
    lateinit var overviewTv: ReadMoreTextView
    lateinit var voteAverageTv: TextView
    lateinit var genresList: String
    lateinit var companiesRecyclerView: RecyclerView
    lateinit var companiesAdapter: ProductionCompaniesAdapter
    lateinit var seasonsRecyclerView: RecyclerView
    lateinit var seasonsAdapter: SeasonsAdapter
    lateinit var seriesVideoBtn: ImageButton
    lateinit var castRecyclerView: RecyclerView
    lateinit var castAdapter: CastAdapter
    lateinit var castList: List<CastProperties>
    lateinit var backBtn: ImageButton
    lateinit var favoriteBtn: ImageButton
    private var addFavorite: Boolean = true
    lateinit var watchlistBtn: ImageButton
    private var addWatchlist: Boolean = true
    lateinit var userPropertiesPreferences: SharedPreferences
    lateinit var progressBar: ProgressBar
    lateinit var blurLayout: LinearLayout
    lateinit var reviewRecyclerView: RecyclerView
    lateinit var reviewAdapter: ReviewAdapter
    lateinit var reviewList: List<ReviewProperties>
    lateinit var reviewsExpandableLayout: ExpandableLayout
    lateinit var reviewsLL: LinearLayout
    lateinit var adView: AdView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.series_detail_fragment, container, false)
        initView(view)
        initAd()
        backPressHandler()
        return view
    }

    private fun initAd() {
        MobileAds.initialize(context, "ca-app-pub-1898235865477290~9103030179")
        val adRequest = AdRequest.Builder().build()
        adView?.loadAd(adRequest)

    }

    private fun initView(view: View) {
        adView = view.findViewById(R.id.ad_banner_series_detail_fragment)
        backdropPathImg = view.findViewById(R.id.backdrop_path_img)
        posterPathImg = view.findViewById(R.id.poster_path_img)
        titleTv = view.findViewById(R.id.title_tv)
        genresTv = view.findViewById(R.id.genres_tv)
        productionCompaniesTv = view.findViewById(R.id.production_companies_tv)
        firstAirDateTv = view.findViewById(R.id.first_air_date_tv)
        voteAverageTv = view.findViewById(R.id.vote_average_tv)
        overviewTv = view.findViewById(R.id.overview_tv)
        backBtn = view.findViewById(R.id.back_btn)

        reviewsLL = view.findViewById(R.id.reviews_ll)
        genresList = ""
        castList = mutableListOf()
        reviewList = mutableListOf()
        seriesVideoBtn = view.findViewById(R.id.series_video_btn)
        favoriteBtn = view.findViewById(R.id.favorite_btn)
        watchlistBtn = view.findViewById(R.id.watchlist_btn)
        castRecyclerView = view.findViewById(R.id.cast_recycler_view)
        castRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        companiesRecyclerView = view.findViewById(R.id.companies_recycler_view)
        companiesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        seasonsRecyclerView = view.findViewById(R.id.seasons_recycler_view)
        seasonsRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        userPropertiesPreferences =
            context!!.getSharedPreferences("userProperties", Context.MODE_PRIVATE)

        reviewRecyclerView = view.findViewById(R.id.review_recycler_view)
        reviewRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        reviewsExpandableLayout = view.findViewById(R.id.reviews_expandable_view)

        progressBar = view.findViewById(R.id.spin_kit)
        blurLayout = view.findViewById(R.id.blur_layout)
        val chasingDots: Sprite = ChasingDots()
        progressBar.setIndeterminateDrawableTiled(chasingDots)

        val seriesId = this.arguments?.getInt("seriesId")
        getSeriesDetail(seriesId!!)
        getVideo(seriesId)
        getCast(seriesId)
        getFavoriteSeries(seriesId)
        getSeriesWatchList(seriesId)
        getReviews(seriesId)
        backBtn.setOnClickListener(this)
        favoriteBtn.setOnClickListener(this)
        watchlistBtn.setOnClickListener(this)
        reviewsLL.setOnClickListener(this)
    }

    private fun getReviews(seriesId: Int) {
        val call: Call<Review> = ApiClient.getClient.getSeriesReviews(seriesId)
        call.enqueue(object : Callback<Review> {
            override fun onFailure(call: Call<Review>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<Review>?, response: Response<Review>?) =
                if (response!!.isSuccessful) {
                    if (response.body().results.isNullOrEmpty()) {
                        reviewsLL.visibility = View.GONE
                    }
                    reviewList = response.body().results
                    reviewAdapter =
                        ReviewAdapter(reviewList, context!!)
                    reviewRecyclerView.adapter = reviewAdapter

                } else {
                }
        }
        )
    }

    private fun getSeriesWatchList(seriesId: Int) {
        val sessionId = userPropertiesPreferences.getString("sessionId", "")
        val accountId = userPropertiesPreferences.getInt("accountId", 0)
        val call: Call<SeriesData> = ApiClient.getClient.getSeriesWatchlist(accountId, sessionId!!)
        call.enqueue(object : Callback<SeriesData> {
            override fun onFailure(call: Call<SeriesData>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<SeriesData>?, response: Response<SeriesData>?) =
                if (response!!.isSuccessful) {
                    for (i in response.body().results) {
                        if (seriesId == i.id) {
                            watchlistBtn.setImageResource(R.drawable.tick)
                            addWatchlist = false
                        }
                    }

                } else {
                }
        }
        )
    }

    private fun getFavoriteSeries(seriesId: Int) {
        val sessionId = userPropertiesPreferences.getString("sessionId", "")
        val accountId = userPropertiesPreferences.getInt("accountId", 0)
        val call: Call<SeriesData> = ApiClient.getClient.getFavoriteSeries(accountId, sessionId!!)
        call.enqueue(object : Callback<SeriesData> {
            override fun onFailure(call: Call<SeriesData>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<SeriesData>?, response: Response<SeriesData>?) =
                if (response!!.isSuccessful) {
                    for (i in response.body().results) {
                        if (seriesId == i.id) {
                            favoriteBtn.setImageResource(R.drawable.add_favorite)
                            addFavorite = false
                        }
                    }

                } else {
                }
        }
        )
    }

    private fun getVideo(movieId: Int) {
        val call: Call<VideoData> = ApiClient.getClient.getVideo(movieId)
        call.enqueue(object : Callback<VideoData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<VideoData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<VideoData>?, response: Response<VideoData>?) =
                if (response!!.isSuccessful) {
                    seriesVideoBtn.setOnClickListener {
                        if (response.body().results.isNullOrEmpty()) {
                            Toast.makeText(context, R.string.video_not_found, Toast.LENGTH_SHORT)
                                .show()
                        } else {
                            val videoKey = response.body().results[0].key
                            val uri = Uri.parse("https://www.youtube.com/watch?v=$videoKey")
                            val intent = Intent(Intent.ACTION_VIEW, uri)
                            startActivity(intent)
                        }

                    }
                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getSeriesDetail(seriesId: Int) {
        val call: Call<SeriesDetail> = ApiClient.getClient.getSeriesDetail(seriesId)
        call.enqueue(object : Callback<SeriesDetail> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<SeriesDetail>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<SeriesDetail>?, response: Response<SeriesDetail>?) =
                if (response!!.isSuccessful) {
                    titleTv.text = response.body().name
                    val urlBackdropPath =
                        "https://image.tmdb.org/t/p/w500" + response.body().backdrop_path
                    picassoImage(backdropPathImg, urlBackdropPath)

                    if (response.body().backdrop_path.isNullOrEmpty())
                        backdropPathImg.setImageResource(R.drawable.default_poster)
                    else
                        picassoImage(backdropPathImg, urlBackdropPath)

                    val urlPosterPath =
                        "https://image.tmdb.org/t/p/w500" + response.body().poster_path

                    if (response.body().poster_path.isNullOrEmpty())
                        posterPathImg.setImageResource(R.drawable.default_poster)
                    else
                        picassoImage(posterPathImg, urlPosterPath)

                    firstAirDateTv.text = response.body().first_air_date
                    voteAverageTv.text = response.body().vote_average.toString()

                    overviewTv.text = response.body().overview



                    for (i in response.body().genres) {
                        if (genresList.isNotEmpty())
                            genresList += ", " + i.name
                        else
                            genresList += i.name
                    }

                    genresTv.text = genresList
                    if (response.body().production_companies.isNullOrEmpty()) {
                        productionCompaniesTv.visibility = View.GONE
                    } else {
                        productionCompaniesTv.visibility = View.VISIBLE
                        companiesAdapter =
                            ProductionCompaniesAdapter(
                                response.body().production_companies,
                                context!!
                            )
                        companiesRecyclerView.adapter = companiesAdapter
                    }
                    seasonsAdapter =
                        SeasonsAdapter(response.body().seasons, context!!, seriesId)
                    seasonsRecyclerView.adapter = seasonsAdapter

                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getCast(seriesId: Int) {
        val call: Call<CastData> = ApiClient.getClient.getSeriesCast(seriesId)
        call.enqueue(object : Callback<CastData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<CastData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<CastData>?, response: Response<CastData>?) =
                if (response!!.isSuccessful) {

                    castList = response.body().cast
                    castAdapter =
                        CastAdapter(castList, context!!)
                    castRecyclerView.adapter = castAdapter
                    blurLayout.visibility = View.GONE
                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    fun picassoImage(ImageView: ImageView, url: String) {
        Picasso.get().load(url).into(ImageView)
    }

    override fun onClick(v: View?) {
        val sessionId = userPropertiesPreferences.getString("sessionId", "")
        when (v?.id) {
            backBtn.id -> {
                Navigation.findNavController(v).popBackStack()
            }
            favoriteBtn.id -> {
                if (sessionId.isNullOrEmpty()) {
                    ViewTooltip
                        .on(this, favoriteBtn)
                        .autoHide(true, 1000)
                        .clickToHide(true)
                        .animation(ViewTooltip.FadeTooltipAnimation(500))
                        .position(ViewTooltip.Position.BOTTOM)
                        .text(R.string.login_tooltip)
                        .textColor(Color.WHITE)
                        .color(Color.BLACK)
                        .corner(30)
                        .arrowWidth(15)
                        .arrowHeight(30)
                        .distanceWithView(0)
                        .show()
                }
                if (addFavorite) {
                    addFavorite = false
                    markAsFavorite(true)

                } else {
                    addFavorite = true
                    markAsFavorite(false)

                }
            }
            watchlistBtn.id -> {
                if (sessionId.isNullOrEmpty()) {
                    ViewTooltip
                        .on(this, watchlistBtn)
                        .autoHide(true, 1000)
                        .clickToHide(true)
                        .animation(ViewTooltip.FadeTooltipAnimation(500))
                        .position(ViewTooltip.Position.BOTTOM)
                        .text(R.string.login_tooltip)
                        .textColor(Color.WHITE)
                        .color(Color.BLACK)
                        .corner(30)
                        .arrowWidth(15)
                        .arrowHeight(30)
                        .distanceWithView(0)
                        .show()
                }
                if (addWatchlist) {
                    addWatchlist = false
                    addToWatchlist(true)
                } else {
                    addWatchlist = true
                    addToWatchlist(false)
                }
            }
            reviewsLL.id -> {
                if (reviewsExpandableLayout.isExpanded) {
                    reviewsExpandableLayout.collapse()

                } else {
                    reviewsExpandableLayout.expand()
                }
            }
        }

    }

    private fun addToWatchlist(isAddToWatchlist: Boolean) {
        val seriesId = this.arguments?.getInt("seriesId")
        var addShowsToWatchlistObject = AddShowsToWatchlist("tv", seriesId!!, isAddToWatchlist)

        val sessionId = userPropertiesPreferences.getString("sessionId", "")
        val accountId = userPropertiesPreferences.getInt("accountId", 0)
        val call: Call<AddtoWatchlist> = ApiClient.getClient.addToWatchlist(
            accountId,
            sessionId!!,
            addShowsToWatchlistObject
        )
        call.enqueue(object : Callback<AddtoWatchlist> {
            override fun onFailure(call: Call<AddtoWatchlist>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<AddtoWatchlist>?,
                response: Response<AddtoWatchlist>?
            ) =
                if (response!!.isSuccessful) {
                    when (response.body().status_code) {
                        1 -> {
                            watchlistBtn.setImageResource(R.drawable.tick)
                        }
                        12 -> {
                            watchlistBtn.setImageResource(R.drawable.tick)
                        }
                        13 -> {
                            watchlistBtn.setImageResource(R.drawable.plus)
                        }
                        else -> {

                        }
                    }

                } else {
                }
        }
        )
    }

    private fun markAsFavorite(isFavorite: Boolean) {
        val seriesId = this.arguments?.getInt("seriesId")
        var markAsFavoriteObject = MarkFavoriteShows("tv", seriesId!!, isFavorite)

        val sessionId = userPropertiesPreferences.getString("sessionId", "")
        val accountId = userPropertiesPreferences.getInt("accountId", 0)


        val call: Call<MarkAsFavorite> = ApiClient.getClient.markAsFavorite(
            accountId,
            sessionId!!,
            markAsFavoriteObject
        )
        call.enqueue(object : Callback<MarkAsFavorite> {
            override fun onFailure(call: Call<MarkAsFavorite>?, t: Throwable?) {
                Toast.makeText(context, R.string.check_internet_connection, Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<MarkAsFavorite>?,
                response: Response<MarkAsFavorite>?
            ) =
                if (response!!.isSuccessful) {
                    when (response.body().status_code) {
                        1 -> {
                            favoriteBtn.setBackgroundResource(R.drawable.add_favorite)
                        }
                        12 -> {
                            favoriteBtn.setBackgroundResource(R.drawable.add_favorite)
                        }
                        13 -> {
                            favoriteBtn.setBackgroundResource(R.drawable.favorite)
                        }
                        else -> {
                        }
                    }
                } else {
                }
        }
        )
    }

    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view!!).popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}